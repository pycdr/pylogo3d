from vpython import *

b_head = box(
	pos=vector(1,15,7.5),
	length = 5,
	height = 5,
	width = 10,
	color = color.blue
)

b_body = box(
	pos=vector(1,10,10),
	length = 5,
	height = 5,
	width = 15,
	color = color.blue
)

b_hand = box(
	pos=vector(1,5,15),
	length = 5,
	height = 5,
	width = 5,
	color = color.blue
)

b_eye = sphere(
	pos=vector(1,14,9),
	radius = 2.75
)
#-------------------------------------------------#
#-------------------------------------------------#
y_head = box(
	pos=vector(1,0,7.5),
	length = 5,
	height = 5,
	width = 10,
	color = color.yellow
)

y_body = box(
	pos=vector(1,5,5),
	length = 5,
	height = 5,
	width = 15,
	color = color.yellow
)

y_hand = box(
	pos=vector(1,10,0),
	length = 5,
	height = 5,
	width = 5,
	color = color.yellow
)

y_eye = sphere(
	pos=vector(1,1,6),
	radius = 2.75
)
#--------------------------------------------------#
#--------------------------------------------------#
about = label(
	pos = vector(5,5,0),
	text = '<python logo (3D)>\npowered by Vpython',
	align = 'left',
	xoffset = 50,
	yoffset = 0,
	border = 2,
	line = True
)
scene.append_to_caption('\nin telegram: <a href="https://t.me/pycdr">my id</a> & <a href="https://t.me/pycdr_pycdr">my channel</a>\nin <a href="https://gitlab.com/pycdr">gitlab</a>')
